/*
 * `app` module
 * ============
 *
 * Provides the game initialization routine.
 */

// Required: import the Babel runtime module.
import 'babel-polyfill';

// Import game states.
import * as states from './states';

export function init() {
  let height = 600;
  let s = height / window.innerHeight;
  let width = window.innerWidth * s;

  const game = new Phaser.Game(width, height, Phaser.AUTO, null, null, true);

  // Dynamically add all required game states.
  Object
    .entries(states)
    .forEach(([key, state]) => game.state.add(key, state));
    
    game.state.start('Boot');



  return game;
}
