/*
 * `assets` module
 * ===============
 *
 * This module declares static Phaser Asset Packs that will be loaded using
 * the `Loader#pack` method.
 *
 * Use this file to declare assets used in your game. Check the commented
 * notes further below for examples of different asset types.
 *
 * Notes:
 *
 * - Loading files are relative to your project's `index.html`. You can
 *   change that using the Phaser.Loader `baseURL` and `path` properties.
 *   Refer to the [Phaser API Documentation][1] to decide what's appropriate
 *   for your needs. Usually, `#path` is your best option. This project
 *   contains an `assets/` folder, configured as your `#path` option, so
 *   declared game assets placed there should load with no problems.
 *
 * - All entries are equivalent to their respective `Phaser.Loader` methods.
 *
 * - Some asset entries may omit their URL field. In this case, a pattern
 *   <key>.<extension> is assumed, that is, the `key` name is appended an
 *   given file extension to form its URL, which depends on what asset
 *   `type` you are declaring.
 *
 *   For graphical assets, PNG is preferred as the default graphic format.
 *
 *   Remember that file names are case sensitive in URLs, thus `*.JPG` and
 *   `*.jpg` are different and might cause trouble.
 *
 * - The URL maybe a `data:` scheme, but these are rather verbose, so use
 *   at your discretion.
 *
 * - Some asset types require a callback function for post processing.
 *   In this case, use the `Loader#pack` fourth argument to specify the
 *   appropriate callback context -- usually, the calling game state.
 *
 *   ```
 *   // Assuming `this` is your game state context.
 *   game.load.pack(key, null, packObject, this);
 *   ```
 *
 * [1]: http://phaser.io/docs
 */

export default {
  // -- Splash screen assets, displayed during the 'Preload' state.
  boot: [{
    key: 'splash-screen',
    type: 'image'
  }, {
    key: 'progress-bar',
    type: 'image'
  }],


  
  World1Stage2: [{
    key: 'gamemap',
    type: 'tilemap',
    url: 'map/world1/stage2/map.json',
    data: null,
    format: 'TILED_JSON',
  }],
  World1Stage3: [{
    key: 'gamemap',
    type: 'tilemap',
    url: 'map/world1/stage3/map.json',
    data: null,
    format: 'TILED_JSON'
  }],




  // -- General assets, used throughout the game.
  game: [{
    key: 'phaser',
    type: 'image'
  },{
    key: 'logo',
    type: 'image'
  },{
    key: 'clear',
    type: 'image'
  },{
    key: 'pin',
    type: 'image'
  },{
    key: 'red',
    type: 'image',
  },{
    key: 'character',
    type: 'spritesheet',
    url: 'sprites/character.png',
    margin: 0,
    spacing: 0,
    // frameMax: 8,
    frameWidth: 101,
    frameHeight: 105
  },{
    key: 'character_physics',
    type: 'physics',
    url: 'character_physics.json'
  }, {
    key: 'objects_physics',
    type: 'physics',
    url: 'physics.json'
  }, {
    key: 'game_controls',
    type: 'spritesheet',
    url: 'sprites/game_controls.png',
    margin: 0,
    spacing: 0,
    // frameMax: 8,
    frameWidth: 50,
    frameHeight: 50
  },{
    key: 'text_frame_w1',
    type: 'image',
  },{
    key: 'text_frame_w2',
    type: 'image',
  },{
    key: 'play_button',
    type: 'image',
  },{
    key: 'exit_agua',
    type: 'image',
  },{
    key: 'exit',
    type: 'image',
  },
  // Menu Assets
  {
    key: 'mountain',
    type: 'image'
  },{
    key: 'worlds_info',
    type: 'json',
  },{
    key: 'zen_character',
    type: 'image',
  },
  // World 1
  // Stage 1: Mundo acuático
  {
    key: 'World1Stage1Map',
    type: 'tilemap',
    url: 'map/world1/stage1/map.json',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'background',
    type: 'image',
    url: 'map/world1/stage1/background.png'
  },{
    key: 'pez_1',
    type: 'spritesheet',
    url: 'sprites/pez_1.png',
    margin: 0,
    spacing: 0,
    // frameMax: 8,
    frameWidth: 47,
    frameHeight: 30
  },{
    key: 'pez_2',
    type: 'spritesheet',
    url: 'sprites/pez_2.png',
    margin: 0,
    spacing: 0,
    // frameMax: 8,
    frameWidth: 71,
    frameHeight: 27
  },{
    key: 'pez_gordo',
    type: 'spritesheet',
    url: 'sprites/pez_gordo.png',
    margin: 0,
    spacing: 0,
    // frameMax: 8,
    frameWidth: 135,
    frameHeight: 36
  },{
    key: 'burbujas',
    type: 'spritesheet',
    url: 'sprites/burbujas.png',
    margin: 0,
    spacing: 0,
    // frameMax: 8,
    frameWidth: 44,
    frameHeight: 64
  },{
    key: 'palabras',
    type: 'spritesheet',
    url: 'sprites/palabras.png',
    margin: 0,
    spacing: 0,
    frameMax: 12,
    frameWidth: 88,
    frameHeight: 49
  },{
    key: 'sfx.jump',
    type: 'audio',
    urls: ['sfx/game/Jump-SoundBible.com-1007297584.mp3']
  },{
    key: 'sfx.coin_bounce',
    type: 'audio',
    urls: ['sfx/game/Coin_Bounce.ogg']
  },{
    key: 'sfx.coin_pickup',
    type: 'audio',
    urls: ['sfx/game/Pickup_Coin8.ogg']
  },{
    key: 'sfx.hit',
    type: 'audio',
    urls: ['sfx/game/CrateHit.ogg']
  },{
    key: 'sfx.backtrack',
    type: 'audio',
    urls: ['sfx/game/JumpingBat.ogg']
  },
  // Stage 2: Juego Completar
  {
    key: 'World1Stage2Map',
    type: 'tilemap',
    url: 'map/world1/stage2/map.json',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'frame_completar',
    type: 'image',
    url: 'minigames/frame_completar.png',
  },{
    key: 'completar',
    type: 'image',
    url: 'minigames/completar.png',
  },{
    key: 'bg_mountain',
    type: 'image',
    url: 'minigames/bg_mountain.png',
  },{
    key: 'fg_mountain',
    type: 'image',
    url: 'minigames/fg_mountain.png',
  },
  // Stage 3: Juego Ahorcado
  {
    key: 'World1Stage3Map',
    type: 'tilemap',
    url: 'map/world1/stage2/map.json',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'frame_ahorcado',
    type: 'image',
    url: 'minigames/frame_ahorcado.png',
  },{
    key: 'ranita',
    type: 'spritesheet',
    url: 'sprites/ranita.png',
    frameWidth: 400,
    frameHeight: 230
  },
  // Stage 4: Juego Dinosaurios
  {
    key: 'World1Stage4Map',
    type: 'tilemap',
    url: 'map/world1/stage4/map.json',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'bg_w1s4',
    type: 'image',
    url: 'map/world1/stage4/background.png',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'fg_w1s4',
    type: 'image',
    url: 'map/world1/stage4/route.png',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'mg_w1s4',
    type: 'image',
    url: 'map/world1/stage4/midground.png',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'dino',
    type: 'spritesheet',
    url: 'sprites/dino.png',
    frameWidth: 270,
    frameHeight: 417
  },{
    key: 'swirl',
    type: 'spritesheet',
    url: 'sprites/swirl.png',
    frameWidth: 70,
    frameHeight: 70
  },{
    key: 'objects_w1',
    type: 'spritesheet',
    url: 'sprites/objects_w1.png',
    frameWidth: 64,
    frameHeight: 64
  },{
    key: 'objects_w2',
    type: 'spritesheet',
    url: 'sprites/objects_w2.png',
    frameWidth: 181,
    frameHeight: 102
  },{
    key: 'frogs',
    type: 'spritesheet',
    url: 'sprites/frogs.png',
    frameWidth: 64,
    frameHeight: 64
  },
  // Stage 5: Juego Shooting
  {
    key: 'World1Stage5Map',
    type: 'tilemap',
    url: 'map/world1/stage5/map.json',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'frame_shooting',
    type: 'image',
    url: 'minigames/frame_shooting.png',
  },{
  // World 2
    key: 'objects_w2',
    type: 'spritesheet',
    url: 'sprites/objects_w2.png',
    frameWidth: 64,
    frameHeight: 64
  },{
    key: 'fortune_wheel',
    type: 'image',
  },{
    key: 'end_frame_w2',
    type: 'image',
  },{
    key: 'exit_door',
    type: 'image',
  },{
  // Stage 1
    key: 'World2Stage1Map',
    type: 'tilemap',
    url: 'map/world2/stage1/map.json',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'box',
    type: 'image',
  },{
    key: 'bg_w2s1',
    type: 'image',
    url: 'map/world2/stage1/bg.png',
  },{
  // Stage 2
    key: 'World2Stage2Map',
    type: 'tilemap',
    url: 'map/world2/stage2/map.json',
    data: null,
    format: 'TILED_JSON'
  },{
    key: 'platform',
    type: 'image',
  },{
    key: 'ruler',
    type: 'image',
  },{
    key: 'ray_shot',
    type: 'image',
  },{
    key: 'bg_w2s2',
    type: 'image',
    url: 'map/world2/stage2/bg.png',
  },{
  // World 3
    key: 'plane',
    type: 'image'
  },{
  // Stage 1: El vuelo
  //   key: 'World3Stage1Map',
  //   type: 'tilemap',
  //   url: 'map/world3/stage1/map.json',
  //   data: null,
  //   format: 'TILED_JSON'
  // },{
    key: 'starfield',
    type: 'image'
  },{
  // World 4
    key: 'moon_module',
    type: 'image'
  },{
    key: 'space_station',
    type: 'image'
  },{
    key: 'gas',
    type: 'image'
  },{
    key: 'controller_w4',
    type: 'image'
  },{
    key: 'planets',
    type: 'spritesheet',
    url: 'sprites/planets.png',
    frameWidth: 258,
    frameHeight: 238
  },{
  // Stage 1: El vuelo
  // 
  // }, {
  //   //  Example: Add an image.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'image',
  //   url: 'example.png'
  // }, {
  //   //  Example: Add a text file.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.txt` is assumed.
  //   //
  //   //  Retrieve the file with `game.cache.getText(<key>)`.
  //   key: 'example',
  //   type: 'text',
  //   url: 'example.txt'
  // }, {
  //   //  Example: Add a JSON document.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.json` is assumed.
  //   //
  //   //  Retrieve the file with `game.cache.getJSON(<key>)`.
  //   key: 'example',
  //   type: 'json',
  //   url: 'example.json'
  // }, {
  //   //  Example: Add a XML document.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.xml` is assumed.
  //   //
  //   //  Retrieve the file with `game.cache.getXML(<key>)`.
  //   key: 'example',
  //   type: 'xml',
  //   url: 'example.xml'
  // }, {
  //   //  Example: Add a custom format, binary file.
  //   //
  //   //  The `url` is mandatory. Requires a callback context.
  //   //
  //   //  Retrieve the file with `game.cache.getBinary(<key>)`.
  //   key: 'example',
  //   type: 'binary',
  //   url: 'example.bin',
  //   callback: 'exampleCallback'
  // }, {
  //   //  Example: Add a spritesheet texture.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'spritesheet',
  //   url: 'example.png',
  //   margin: 0,
  //   spacing: 0,
  //   frameMax: 8,
  //   frameWidth: 32,
  //   frameHeight: 32
  // }, {
  //   //  Example: Add video.
  //   //
  //   //  Supply `urls` for one of several files in different formats.
  //   key: 'example',
  //   type: 'video',
  //   urls: ['example.m4v', 'example.webm']
  // }, {
  //   //  Example: Add audio.
  //   //
  //   //  Supply `urls` for one of several files in different formats.
  //   key: 'example',
  //   type: 'audio',
  //   urls: ['example.m4a', 'example.oga']
  // }, {
  //   //  Example: Add an audio sprite with some sound effects.
  //   //
  //   //  Supply `urls` for one of several files in different formats.
  //   //
  //   //  The mandatory `jsonURL` field specifies the audio sprites data.
  //   key: 'example',
  //   type: 'audiosprite',
  //   urls: ['example.m4a', 'example.oga'],
  //   jsonURL: 'example.json'
  // }, {
  //   //  Example: Add a Tiled tilemap.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.csv` or `<key>.json` is assumed.
  //   //
  //   //  The `format` field specifies in which data format your tilemap was
  //   //  exported, either `CSV` or `TILED_JSON`.
  //   //
  //   //  Use different `image` pack entries to load the necessary textures.
  //   key: 'example',
  //   type: 'tilemap',
  //   url: 'example.json',
  //   format: 'TILED_JSON'
  // }, {
  //   //  Example: Add a Lime+Corona physics data file.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.json` is assumed.
  //   key: 'example',
  //   type: 'physics',
  //   url: 'example.json'
  // }, {
  //   //  Example: Add a retro, bitmap font.
  //   //
  //   //  If `atlasURL` is omitted, a pattern `<key>.json` is assumed.
  //   //
  //   //  If `textureURL` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'bitmapFont',
  //   atlasURL: 'example.json',
  //   textureURL: 'example.png',
  //   xSpacing: 0,
  //   ySpacing: 0
  // }, {
  //   //  Example: Add a texture atlas.
  //   //
  //   //  Use the `format` field to specify the texture atlas data format:
  //   //  - `TEXTURE_ATLAS_XML_STARLING`: Starling XML data format.
  //   //  - `TEXTURE_ATLAS_JSON_HASH`: JSON Hash data format.
  //   //  - `TEXTURE_ATLAS_JSON_ARRAY`: JSON Array data format (default).
  //   //
  //   //  If `atlasURL` is omitted, a pattern `<key>.json` (or `<key>.xml`) is
  //   //  assumed.
  //   //
  //   //  If `textureURL` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'atlas',
  //   atlasURL: 'example.json',
  //   textureURL: 'example.png',
  //   format: 'TEXTURE_ATLAS_JSON_HASH'
  // }, {
  //   //  Example: Add a texture atlas (alternative form).
  //   //
  //   //  Use the `type` field to specify the texture atlas format, as follows:
  //   //  - `atlasXML`: Starling XML Texture Atlas data format.
  //   //  - `atlasJSONHash`: texture atlas in JSON Hash data format.
  //   //  - `atlasJSONArray`: texture atlas in JSON Array data format.
  //   //
  //   //  If `atlasURL` is omitted, a pattern `<key>.json` (or `<key>.xml`) is
  //   //  assumed.
  //   //
  //   //  If `textureURL` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'atlasJSONHash',
  //   atlasURL: 'example.json',
  //   textureURL: 'example.png'
  }]
};
