/*
 * World1Stage5 state
 *
 * Juego Shooting
 */
 import GameMap from '../objects/GameMap';
 import Character from '../objects/Character';
 import MyGameController from '../objects/MyGameController';

export default class World1Stage5 extends Phaser.State {

  constructor() {
    // TODO: Stub
    super();
    this.map = null;

    this.names = ['parlante', 'cubo', 'radio'];
    this.legends = {
      cubo: 'Generamos opinión pública informada y deliberantes.',
      parlante: 'El mayor proyecto de difusión y promoción científica y tecnológica de Medellín',
      radio: 'Somos un centro interactivo para la apropiación y la divulgación de la ciencia y la tecnología.'
    };
    this.countCollects = 0;
  }

  create() {
    // TODO: Stub
    // Load Game Map
    this.map = new GameMap(this.game);


    this.gameController = new MyGameController(this.game);
    this.loadGameElements();
    this.gameController.setupElements();

    
    let player = this.add.existing(new Character(this.game, 10, this.game.height - 25));
    this.gameController.setupPlayer(player);
    this.gameController.ignoreObstacles = true;


    // TODO: Stub
    this.game.time.events.add(Phaser.Timer.SECOND * 1, this.setupShooting, this);
    this.resize();
  }
  loadGameElements() {
    // Declare Custom Game Elements
    let background = this.game.add.group();
    this.borders = this.game.add.group();
    let frames = this.game.add.group();
    let start = this.game.add.group();

    this.objects = this.game.add.group();
    this.objects.enableBody = true;

    this.currentObject = this.game.add.group();

    frames.enableBody = true;
    this.borders.enableBody = true;

    // Load Controller elements
    this.map.createFromObjects('Background', 2, 'clear', null, true, false, this.gameController.route);
    // Current game Elements
    this.map.createFromObjects('Background', 3, 'bg_mountain', null, true, false, background);
    this.map.createFromObjects('Background', 4, 'fg_mountain', null, true, false, background);
    this.map.createFromObjects('Frame', 2, 'clear', null, true, false, this.borders);
    this.map.createFromObjects('Foreground', 1, 'frame_shooting', null, true, false, frames);
    this.map.createFromObjects('Foreground', 2, 'clear', null, true, false, start);
    this.map.createFromObjects('Foreground', 5, 'exit', null, true, false, this.gameController.exit);


    frames.setAll('body.allowGravity', false);
    frames.setAll('body.immovable', true);
    this.borders.setAll('body.allowGravity', false);
    this.borders.setAll('body.immovable', true);
    this.borders.setAll('body.allowRotation', true);
    this.borders.forEach(function(o) {
      o.body.allowRotation = true;
      o.body.angle = o.angle;
    }, this);

    this.game.world.bringToTop(background);
    this.game.world.bringToTop(frames);
    this.game.world.bringToTop(this.gameController.exit);
    this.game.world.bringToTop(this.objects);

    this.frame = frames.getAt(0);
    this.start = start.getAt(0);


    
    
  }
  
  setupShooting() {
    let names = this.names;

    let objects = {};
    let s = {
      x: this.frame.x + 71,
      y: this.frame.y + this.frame.height - 116
    }

    let bmd = this.game.add.bitmapData(70, 70);
    bmd.ctx.fillStyle = '#FFFFFF';

    // bmd.ctx.setFillColor('#FFFFFF', 0,6);
    bmd.ctx.beginPath();
    bmd.ctx.arc(35, 35, 35, 0, 2*Math.PI, true); 
    bmd.ctx.closePath();
    bmd.ctx.fill();

    for (let i = 0; i < names.length; i++) {
      let k = names.length - i - 1;
      let b = this.objects.create(s.x - k, s.y - 33*k, bmd);
      b.name = names[i];
      b.anchor.set(0.5, 0.5);
      b.scale.setTo(0.4, 0.4);
      b.body.moves = false;

      let o = this.objects.create(0, 0, 'frogs', i);
      o.name = names[i];
      o.anchor.set(0.5, 0.5);
      o.body.immovable = true;
      o.body.allowGravity = false;

      b.addChild(o);


      objects[b.name] = b;

    }
    let i=0;
    let j=0;
    for (let key in objects) {
      this.game.time.events.add(Phaser.Timer.SECOND * 2 * (j + 1), function() {
        let name = names[i];
        let o = objects[name];
        let s = {
          x: this.frame.x + 60,
          y: this.frame.y + 110        }
        let start = this.game.add.tween(o).to({
          x: s.x,
          y: s.y
        }, 500, Phaser.Easing.Linear.None, true);
        start.onComplete.add(function() {
          o.x = this.start.x + this.start.width/2;
          o.y = this.start.y + this.start.height/2;
          o.scale.setTo(0.6, 0.6);
          this.game.physics.arcade.enable(o);
          
          o.inputEnabled = true;
          o.input.useHandCursor = true;
          o.input.pixelPerfectClick = true;
          o.input.snapOffsetX = 200;
          o.events.onInputDown.add(this.collectElement, this);

          o.body.bounce.setTo(1,1);
          o.body.velocity.x = this.game.rnd.integerInRange(70,400);
          o.body.velocity.y = this.game.rnd.integerInRange(70,400);
          o.body.allowGravity = false;
          o.body.moves = true;
          o.body.setCircle(o.width/2 + 3);
        }, this);
        i++;
      }, this);
      j++;
    }
  }
  
  collectElement(parent) {
    if(this.currentObject) {
      this.currentObject.callAll('kill');
    }
    let object = parent.getChildAt(0);
    this.countCollects ++;
    var legend = this.legends[object.name];

    this.currentObject = this.game.add.group();
    var o = this.game.add.sprite(parent.x, parent.y, object.key, object.frame);
    o.anchor.set(0.5, 0.5);
    o.scale.setTo(0.75, 0.75);
    

    // object.kill();
    parent.kill();

    let s = {
      x: this.frame.x + 180,
      y: this.frame.y + this.frame.height - 130
    };
    var style = { font: '11pt didact', fill: '#2f249c', align: 'left', wordWrap: true, wordWrapWidth: 250 };
    var text = this.game.add.text(s.x + 150, s.y, legend, style);
    text.alpha = 0;
    text.anchor.set(0.5, 0.5);
    this.game.add.tween(text).to({
      alpha: 1,
    }, 2000, null, true);

    this.currentObject.addChild(o);
    this.currentObject.addChild(text);

    this.game.add.tween(o).to({
      x: s.x,
      y: s.y,
      width: 64,
      height: 64
    }, 2000, Phaser.Easing.Bounce.Out, true);


    if(this.countCollects == this.names.length) {
      this.gameController.finished = true;
      if(this.fxFinished) this.fxFinished.play();
      // this.exit.alpha = 1;
      // this.game.add.tween(this.exit).to({
      //   height: 100,
      //   alpha: 1
      // }, 1000, Phaser.Easing.Exponential.Out, true);
    }
  }
  
  render() {
    // this.objects.forEachAlive(this.renderGroup, this);
    // this.borders.forEachAlive(this.renderGroup, this);
  }

  renderGroup(member) {
    this.game.debug.body(member);
  }

  update() {
    // TODO: Stub
    this.gameController.update();
    this.game.physics.arcade.collide(this.objects, this.borders);
  }
  resize() {
    this.gameController.resize();
  }

}
