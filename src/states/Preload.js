/*
 * Preload state
 * =============
 *
 * Takes care of loading the main game assets, including graphics and sound
 * effects, while displaying a splash screen with a progress bar, showing how
 * much progress were made during the asset load.
 */

import assets from '../assets';

export default class Preload extends Phaser.State {

  preload() {
    this.isFinished = false;
    this.showSplashScreen();
    this.load.pack('game', null, assets);
  }

  create() {
    // Here is a good place to initialize plugins that depend on any game
    // asset. Don't forget to `import` them first. Example:
    //this.add.plugin(MyPlugin/*, ... initialization parameters ... */);
    
    // Add Global Phaser Plugins
    this.game.plugins.add(Fabrique.Plugins.Responsiveness);
    this.game.stateTransition = this.game.plugins.add(Phaser.Plugin.StateTransition);
    this.game.stateTransition.configure({
      duration: Phaser.Timer.SECOND * 0.8,
      ease: Phaser.Easing.Exponential.InOut,
      properties: {
        alpha: 0,
        scale: {
          x: 1.4,
          y: 1.4
        }
      }
    });
  }
  update() {
    if(this.isFinished == true) {
      this.state.start('Menu');
    }
  }
  // --------------------------------------------------------------------------

  showSplashScreen() {
    this.splash = this.add.image(0, 0, 'splash-screen');
    this.splash.anchor.setTo(0.5);
    this.load.setPreloadSprite(this.add.image(0, 56, 'progress-bar'));
    
    this.load.preloadSprite.sprite.anchor.setTo(0.5);
    this.load.onLoadStart.add(this.loadStart, this);
    this.load.onFileComplete.add(this.fileComplete, this);
    this.load.onLoadComplete.add(this.loadComplete, this);

    this.resize();
  }
  resize() {
    this.load.preloadSprite.sprite.x = this.world.centerX;
    this.load.preloadSprite.sprite.y = this.world.centerY + 56;
    this.splash.x = this.world.centerX;
    this.splash.y = this.world.centerY;
    
    this.scale.setUserScale(this.game.height/600);
  }
  loadStart() {
    // console.log('Start Preload');
  }
  fileComplete(progress, cacheKey, success, totalLoaded, totalFiles) {
    // console.log('File Completed');
  }
  loadComplete() {
    // console.log('Finished Preload');
    this.isFinished = true;
  }

}
