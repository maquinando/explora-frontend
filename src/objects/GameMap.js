/*
 * GameMap
 *
 * Is the map of the World 1, Stage 1 Game. Water Game.
 */

export default class GameMap extends Phaser.Tilemap {

  constructor(game, key) {

    key = key || game.state.current + 'Map';

    super(game, key);

    // this = this.game.add.tilemap(key);
    var gameLayer = this.createBlankLayer('gameLayer', this.width, this.height, this.tileWidth, this.tileHeight);
    gameLayer.resizeWorld();



    // TODO:
    //   1. Edit constructor parameters accordingly.
    //   2. Adjust object properties.
  }
}
