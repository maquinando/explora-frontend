/*
 * Settings
 *
 * Game Settings From Local Storage
 */

export default class Settings extends Map {

  constructor() {
    if(localStorage.mySettings) {
      var entries = JSON.parse(localStorage.mySettings);
      super(entries);
    } else {
      super();
      this
      .set('sound', true);
    }
    // TODO:
    //   1. Edit constructor parameters accordingly.
    //   2. Adjust object properties.
  }
  set_var(key, value) {
    this.set(key, value);
    localStorage.mySettings = JSON.stringify(Array.from(this.entries()));
  }

}
