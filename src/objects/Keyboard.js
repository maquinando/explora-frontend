/*
 * Keyboard
 *
 * Ahorcado game Keyboard
 */

export default class Keyboard extends Phaser.Group {

  constructor(game, callback, x, y, width, height) {
    super(game);

    // TODO:
    //   1. Edit constructor parameters accordingly.
    //   2. Adjust object properties.
    this.callback = callback;
    this.rect = {
      width: width ||500,
      height: height || 350,
      x: x || game.width/2,
      y: y || game.height/2,
    };
    this.letters=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
  }

  refresh() {
    this.removeAll(true, true);

    let w = 20;
    let h = 26;
    let bmd = this.game.add.bitmapData(w, h);
    bmd.ctx.beginPath();
    bmd.ctx.rect(0,0,w,h);
    bmd.ctx.fillStyle = '#da173a';
    bmd.ctx.fill();

    let startPoint = {
      x: this.rect.x,
      y: this.rect.y
    }

    let k = 0;
    for(let i=0; i <this.letters.length; i++)
    {
      let l = this.letters[i].toUpperCase();
      if(l == 'M' || l == 'U') {
        k++;
        startPoint.x = this.rect.x + 50*k;
        if(k==2) startPoint.x -= 25;
        startPoint.y += 30;
      }
      let rectangle = this.game.add.sprite(startPoint.x, startPoint.y, bmd);
      rectangle.anchor.set(0.5, 0.5);
      rectangle.letter = l;

      let letter = this.game.add.text(0, 5, l, {
        font: '16px obelixpro',
        fill: '#FFFFFF',
        align: 'center'
      });
      letter.anchor.set(0.5, 0.5);
      rectangle.inputEnabled = true;
      rectangle.events.onInputDown.add(this.keyPressed, this);
      rectangle.addChild(letter)
      this.add(rectangle);
      
      startPoint.x += 25;
    }

  }
  keyPressed(button) {
    let c = button.letter;
    button.kill();

    this.callback(c);
  }

}
